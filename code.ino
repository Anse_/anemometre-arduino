int pinBouton=8; /* Pin utilisé dans notre circuit, un bouton est utilisé à la place d'un ILS avec aimant car le simulateur tinkercard n'en propose pas*/
long T=10000; /* Variable de départ du temps en ms (soit 10s), laisse le temps au programme de calculer le nombre de tours que fait l'aimant */
long r=0; /* Variable qui compte le nombre de tours effectués*/
long toursminute; /* Variable qui convertis le nombre de tours effectués en 10s en tours/minute */ 
float rads; /* Vitesse angulaire */
float branche=0.04; /* Rayon de la roue en mètre, donnée dans le TP */
float vitesse_s; /* Vitesse en m.s^-1 */
float vitesse_h; /* Vitesse en km.h^-1 */

int Pinledv, Pinledj, Pinledo, Pinledr;

#include <LiquidCrystal.h> 
LiquidCrystal lcd(13, 12, 11, 10, 9, 7);
/*Librairie pour l'écran LCD*/

void setup(){
  lcd.begin(16, 2);
  lcd.print("Initialisation...");
  Pinledv=2;
  Pinledj=3;
  Pinledo=4;
  Pinledr=5;
  
  pinMode(pinBouton, OUTPUT);
  Serial.begin(9600);
  
  pinMode(Pinledv, OUTPUT);
  pinMode(Pinledj, OUTPUT);
  pinMode(Pinledo, OUTPUT);
  pinMode(Pinledr, OUTPUT);
    
  digitalWrite(Pinledv,HIGH); 
  digitalWrite(Pinledj,LOW);
  digitalWrite(Pinledo,LOW);
  digitalWrite(Pinledr,LOW);
}

void loop(){

  digitalWrite(Pinledv,HIGH);
  digitalWrite(Pinledj,HIGH);
  digitalWrite(Pinledo,HIGH);
  digitalWrite(Pinledr,HIGH);
  if (vitesse_h<100)
  {
    if (vitesse_h>=0)
    {
  digitalWrite(Pinledv,HIGH);
  digitalWrite(Pinledj,LOW);
  digitalWrite(Pinledo,LOW);
  digitalWrite(Pinledr,LOW);
    }
      if (vitesse_h>=25)
    {
  digitalWrite(Pinledv,LOW);
  digitalWrite(Pinledj,HIGH);
  digitalWrite(Pinledo,LOW);
  digitalWrite(Pinledr,LOW);
    }  
        if (vitesse_h>=50)
    {
  digitalWrite(Pinledv,LOW);
  digitalWrite(Pinledj,LOW);
  digitalWrite(Pinledo,HIGH);
  digitalWrite(Pinledr,LOW);
    }
       if (vitesse_h>=75)
    {
  digitalWrite(Pinledv,LOW);
  digitalWrite(Pinledj,LOW);
  digitalWrite(Pinledo,LOW);
  digitalWrite(Pinledr,HIGH);
    } 
  }
    
  
  while(millis()<T){
  boolean etatBouton=digitalRead(pinBouton);
    if(etatBouton == 1)
    {
      r=r+1;
      delay(10);
    }
  
  /*Serial.println(etatBouton);*/
  etatBouton=0;
  }
    T=T+10000;
  
  	toursminute=r*6;	  	
  	
  	rads=((2*3.14)/60.0)*float(toursminute);
  
  	vitesse_s=(rads * float(branche));
    vitesse_h=vitesse_s *3.6; 
  	
      
      
      
  	Serial.println("****************************");
    Serial.println("**********CALCULS***********");
  	Serial.print("r =");
  	Serial.println(r);
    
    Serial.print("Nombre de tours =");
  	Serial.println(toursminute);
  
  	Serial.print("rad = ");
	Serial.println(rads);
  
	Serial.print("Vent = ");
    Serial.print(vitesse_s);
    Serial.println("m/s");
                 
    Serial.print("Vitesse du vent en km/h = ");
    Serial.print(vitesse_h);
	Serial.println("km/h");
    Serial.println("****************************");
  	Serial.println("");
    
    lcd.setCursor(0,0); // Affiche le résultat sur , l'écran LCD
    lcd.clear();
  	lcd.print("Vitesse du vent : ");
  	lcd.setCursor(0,1);
	lcd.print(vitesse_h);
	lcd.print(" km/h");
    r=0;
    delay(0); /* À modifier si besoin. Ne pas alors oublier de modifier "T=T+<valeur>" */
}
